<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php wp_body_open(); ?>
				<header class="p-3 text-white">
    <div class="container">
    	<!--SITE BRANDING-->
      <div class="site-branding d-flex flex-wrap align-items-center justify-content-center">
 			<?php the_custom_logo();
				if( is_front_page() && is_home() ):?>
				<?php else: ?>
				<h1><a class="mb-2 mb-lg-0 text-white text-decoration-none" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
				<?php endif;?>
			</div>
				
        <div>
        					<!--MAIN NAVIGATION-->
  						<?php 
					wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'menu_id' 		 => 'primary-menu',
							'container_class'=> 'mx-auto',
							'menu_class'	 => 'nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0'
						)
					);
				?>
        </div>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input type="search" class="form-control form-control-dark" placeholder="Search..." aria-label="Search">
        </form>

      </div>
    </div>
  </header>
		<main id="primary" class="site-main" role="main">